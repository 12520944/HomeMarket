﻿namespace HomeMarket.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NhanDonHang")]
    public partial class NhanDonHang
    {

        public int Id { get; set; }

        [Display(Name = "Mã đơn hàng")]
        public int DonHangId { get; set; }

        [Display(Name = "Mã người đi chợ")]
        public int NguoiDiChoId { get; set; }

        [Display(Name = "Thời gian nhận")]
        public DateTime ThoiGianNhan { get; set; }

        public bool Status { get; set; }
    }
}
