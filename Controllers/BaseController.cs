﻿using HomeMarket.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HomeMarket.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        //protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        //{
        //    base.Initialize(requestContext);
        //    if (Session[CommonConstants.CurrentCulture] != null)
        //    {
        //        Thread.CurrentThread.CurrentCulture = new CultureInfo(Session[CommonConstants.CurrentCulture].ToString());
        //        Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session[CommonConstants.CurrentCulture].ToString());
        //    }
        //    else
        //    {
        //        Session[CommonConstants.CurrentCulture] = "vi";
        //        Thread.CurrentThread.CurrentCulture = new CultureInfo("vi");
        //        Thread.CurrentThread.CurrentUICulture = new CultureInfo("vi");
        //    }
        //}

        //// changing culture
        //public ActionResult ChangeCulture(string ddlCulture, string returnUrl)
        //{
        //    Thread.CurrentThread.CurrentCulture = new CultureInfo(ddlCulture);
        //    Thread.CurrentThread.CurrentUICulture = new CultureInfo(ddlCulture);

        //    Session[CommonConstants.CurrentCulture] = ddlCulture;
        //    return Redirect(returnUrl);
        //}

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var session = (UserLogin)Session[CommonConstants.USER_SESSION];
            if (session == null)
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "Login", action = "Index" }));
            }
            base.OnActionExecuting(filterContext);
        }

    }
}