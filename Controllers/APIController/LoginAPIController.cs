﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using HomeMarket.Models;
using HomeMarket.Common;
namespace HomeMarket.Controllers.APIController
{
    public class LoginAPIController : ApiController
    {
        private HomeMarketDbContext db = new HomeMarketDbContext();

        [ResponseType(typeof(KhachHang))]
        public IHttpActionResult Login(KhachHang khachHang)
        {
            var res = db.KhachHang.Where(x => x.UserName == khachHang.UserName).Count();
            var result = db.KhachHang.SingleOrDefault(x => x.UserName == khachHang.UserName);
            if (res==0)
            {
                return Json("0");
            }
            else
            {
                if (result.Password == Encryptor.MD5Hash(khachHang.Password))
                {
                    db.Entry(result).State = EntityState.Modified;
                    result.RegistrationId = khachHang.RegistrationId;
                    db.SaveChanges();
                    return Json("1");                    
                }
                    
                
                else
                    return Json("-1");
            }
        }
    }
}
