﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeMarket.Models;
using PagedList;

namespace HomeMarket.Controllers
{
    public class NhanDonHangController : Controller
    {
        private HomeMarketDbContext db = new HomeMarketDbContext();

        // GET: /NhanDonHang/
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page){

		   var nhandonhang = from s in db.NhanDonHang select s;				   
 	
			 //Search            
            if (searchString != null) {
                page = 1;
            }
            else {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
				//AttributeSearch, AttributeSearch2 là những trường sẽ đem so sánh với nội dung tìm kiếm, cần thay đổi cho phù hợp
                //nhandonhang = nhandonhang.Where(s => s.D.Contains(searchString)
                //                       || s.AttributeSearch2.Contains(searchString));
            }
            
            //Sort: AttributeSort là thuộc tính để sắp xếp, sẽ đi 1 căp AttributeSort và AttributeSort_desc,
			// có thể có nhiều hơn 1 thuộc tính có thể sắp xếp => cần thay đổi cho phù hợp
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NguoiDiChoId = sortOrder == "NguoiDiChoId" ? "NguoiDiChoId_desc" : "NguoiDiChoId";
            ViewBag.ThoiGianNhan = sortOrder == "ThoiGianNhan" ? "ThoiGianNhan_desc" : "ThoiGianNhan";
            switch (sortOrder)
            {
                case "NguoiDiChoId":
                    nhandonhang = nhandonhang.OrderBy(s => s.NguoiDiChoId);
                    break;
                case "NguoiDiChoId_desc":
                    nhandonhang = nhandonhang.OrderByDescending(s => s.NguoiDiChoId);
                    break;               
                case "ThoiGianNhan":
                    nhandonhang = nhandonhang.OrderBy(s => s.ThoiGianNhan);
                    break;
                default: 
                    nhandonhang = nhandonhang.OrderByDescending(s => s.ThoiGianNhan);
                    break;
            }

            //Pagination
            int pageSize = 10;
            int pageNumber = (page ?? 1);
			
			return View(nhandonhang.ToPagedList(pageNumber, pageSize));

        }//End Index Actions

        // GET: /NhanDonHang/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NhanDonHang nhanDonHang = db.NhanDonHang.Find(id);
            if (nhanDonHang == null)
            {
                return HttpNotFound();
            }
            return View(nhanDonHang);
        }

        // GET: /NhanDonHang/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /NhanDonHang/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,DonHangId,NguoiDiChoId,ThoiGianNhan,Status")] NhanDonHang nhanDonHang)
        {
            if (ModelState.IsValid)
            {
                db.NhanDonHang.Add(nhanDonHang);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(nhanDonHang);
        }

        // GET: /NhanDonHang/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NhanDonHang nhanDonHang = db.NhanDonHang.Find(id);
            if (nhanDonHang == null)
            {
                return HttpNotFound();
            }
            return View(nhanDonHang);
        }

        // POST: /NhanDonHang/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,DonHangId,NguoiDiChoId,ThoiGianNhan,Status")] NhanDonHang nhanDonHang)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nhanDonHang).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(nhanDonHang);
        }

        // GET: /NhanDonHang/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NhanDonHang nhanDonHang = db.NhanDonHang.Find(id);
            if (nhanDonHang == null)
            {
                return HttpNotFound();
            }
            return View(nhanDonHang);
        }

        // POST: /NhanDonHang/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NhanDonHang nhanDonHang = db.NhanDonHang.Find(id);
            db.NhanDonHang.Remove(nhanDonHang);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
