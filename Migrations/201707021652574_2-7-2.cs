namespace HomeMarket.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _272 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NhaCungUngs", "Status", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NhaCungUngs", "Status");
        }
    }
}
