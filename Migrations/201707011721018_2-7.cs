namespace HomeMarket.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _27 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PhanHoi", "Ten", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PhanHoi", "Ten");
        }
    }
}
