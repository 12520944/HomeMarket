namespace HomeMarket.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _271 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PhanHoi", "KhachHangId", "dbo.KhachHang");
            DropIndex("dbo.PhanHoi", new[] { "KhachHangId" });
            AlterColumn("dbo.PhanHoi", "KhachHangId", c => c.Int(nullable: false));
            CreateIndex("dbo.PhanHoi", "KhachHangId");
            AddForeignKey("dbo.PhanHoi", "KhachHangId", "dbo.KhachHang", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PhanHoi", "KhachHangId", "dbo.KhachHang");
            DropIndex("dbo.PhanHoi", new[] { "KhachHangId" });
            AlterColumn("dbo.PhanHoi", "KhachHangId", c => c.Int());
            CreateIndex("dbo.PhanHoi", "KhachHangId");
            AddForeignKey("dbo.PhanHoi", "KhachHangId", "dbo.KhachHang", "Id");
        }
    }
}
